import { Module } from '@nestjs/common';
import { YiJianModule } from './modules/yijian/yijian.module';
import { CommonModule } from './modules/common/common.module';
import { OnlineModule } from './modules/online/online.module';
import { MongooseModule } from '@nestjs/mongoose';
import { JingweiModule } from './modules/jingwei/jingwei.module';
import { UserModule } from './modules/user/user.module';
import { ChatModule } from './modules/chat/chat.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb://127.0.0.1:27017/yiyun-games',
      {
        useNewUrlParser: true,
      },
    ),
    YiJianModule,
    CommonModule,
    OnlineModule,
    JingweiModule,
    UserModule,
    ChatModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
