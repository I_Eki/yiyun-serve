import Axios from 'axios';

const http = Axios.create();

http.interceptors.response.use((res) => {
  if (res.status !== 200) return null;
  return res;
});

export default http;
