import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { FullLingDto, LingDto } from './dto/ling.dto';
import { ShiziDto, TianhaiDto } from './dto/shizi.dto';
import { JingweiService } from './jingwei.service';

@ApiTags('Jingwei')
@Controller('jingwei')
export class JingweiController {
  constructor(private readonly jingweiService: JingweiService) {}

  @Post('addLing')
  async addLing(@Body() body: LingDto) {
    const res: FullLingDto = await this.jingweiService.addLing(body);
    return res;
  }

  @Get('shizi')
  async queryShizi(@Query() query: IDDto) {
    const res = await this.jingweiService.queryShizi(query);
    return res;
  }

  @Post('shizi')
  async addShizi(@Body() body: ShiziDto) {
    const res = await this.jingweiService.addShizi(body);
    return res;
  }

  @Post('turn')
  async turnIntoYandi(@Body() body: IDDto) {
    const res = await this.jingweiService.turnIntoYandi(body);
    return res;
  }

  @Post('tianhai')
  async tianhai(@Body() body: TianhaiDto) {
    const res = await this.jingweiService.tianhai(body);
    return res;
  }
}
