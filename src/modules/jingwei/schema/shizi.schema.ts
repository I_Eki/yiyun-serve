import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaType } from 'mongoose';

export type ShiziDocument = Shizi & Document;

@Schema()
export class Shizi extends Document {
  @Prop({
    required: true,
    type: SchemaType.Types.String,
  })
  title: string;

  @Prop({
    required: true,
    type: SchemaType.Types.String,
    default: '',
  })
  content: string;

  @Prop({
    required: false,
    type: SchemaType.Types.String,
    default: '',
  })
  answer: string;

  @Prop({
    required: true,
    type: SchemaType.Types.ObjectId,
    ref: 'Ling',
  })
  jingwei: string;

  @Prop({
    required: true,
    type: SchemaType.Types.ObjectId,
    ref: 'Ling',
  })
  yandi: string;
}

export const ShiziSchema = SchemaFactory.createForClass(Shizi);
