import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaType } from 'mongoose';
import { Hun } from '../dto/ling.dto';

export type LingDocument = Ling & Document;

@Schema()
export class Ling extends Document {
  @Prop({
    required: true,
    type: SchemaType.Types.String,
  })
  name: string;

  @Prop({
    required: true,
    type: SchemaType.Types.Number,
  })
  hun: Hun;

  @Prop({
    required: true,
    type: SchemaType.Types.ObjectId,
    ref: 'Ling',
    default: null,
  })
  ling: string;
}

export const LingSchema = SchemaFactory.createForClass(Ling);
