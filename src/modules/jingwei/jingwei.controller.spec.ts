import { Test, TestingModule } from '@nestjs/testing';
import { JingweiController } from './jingwei.controller';

describe('JingweiController', () => {
  let controller: JingweiController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [JingweiController],
    }).compile();

    controller = module.get<JingweiController>(JingweiController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
