export class ShiziDto {
  title: string;
  content: string;
  answer?: string;
  jingwei: string;
  yandi: string;
}

export class TianhaiDto {
  _id: string;
  answer: string;
}
