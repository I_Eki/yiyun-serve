export enum Hun {
  YANDI,
  JINGWEI,
}

export class LingDto {
  name: string;
  hun: Hun;
  ling: string;
}

export class YandiDto extends LingDto {
  hun: Hun.YANDI;
}

export class JingweiDto extends LingDto {
  hun: Hun.JINGWEI;
}

export type FullLingDto = IDDto & LingDto;
