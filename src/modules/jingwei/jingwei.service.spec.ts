import { Test, TestingModule } from '@nestjs/testing';
import { JingweiService } from './jingwei.service';

describe('JingweiService', () => {
  let service: JingweiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [JingweiService],
    }).compile();

    service = module.get<JingweiService>(JingweiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
