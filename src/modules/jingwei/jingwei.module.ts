import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JingweiService } from './jingwei.service';
import { Ling, LingSchema } from './schema/ling.schema';
import { Shizi, ShiziSchema } from './schema/shizi.schema';
import { JingweiController } from './jingwei.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Ling.name,
        schema: LingSchema,
      },
      {
        name: Shizi.name,
        schema: ShiziSchema,
      },
    ]),
  ],
  providers: [JingweiService],
  controllers: [JingweiController],
})
export class JingweiModule {}
