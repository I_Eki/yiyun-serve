import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Ling, LingDocument } from './schema/ling.schema';
import { Shizi, ShiziDocument } from './schema/shizi.schema';
import { Hun, LingDto } from './dto/ling.dto';
import { ShiziDto, TianhaiDto } from './dto/shizi.dto';

@Injectable()
export class JingweiService {
  constructor(
    @InjectModel(Ling.name) private readonly lingModel: Model<LingDocument>,
    @InjectModel(Shizi.name) private readonly shiziModel: Model<ShiziDocument>,
  ) {}

  addLing(dto: LingDto) {
    return this.lingModel.create(dto);
  }

  async turnIntoYandi(dto: IDDto) {
    const shiziList = await this.shiziModel
      .find({ jingwei: dto._id, answer: '' })
      .exec();
    if (shiziList.length < 1) return;

    return this.lingModel
      .updateOne({ _id: dto._id }, { hun: Hun.YANDI })
      .exec();
  }

  addShizi(dto: ShiziDto) {
    return this.shiziModel.create(dto);
  }

  tianhai(dto: TianhaiDto) {
    return this.shiziModel
      .updateOne({ _id: dto._id }, { answer: dto.answer })
      .exec();
  }

  queryShizi(dto: IDDto) {
    return this.shiziModel.find({ yandi: dto._id });
  }
}
