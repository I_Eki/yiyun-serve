import { Injectable } from '@nestjs/common';
import * as Core from '@alicloud/pop-core';
import http from 'src/utils/http';

@Injectable()
export class CommonService {
  private readonly client: Core;
  private readonly clientParams: unknown;

  constructor() {
    this.client = new Core({
      accessKeyId: 'LTAI5tJyzZyNBksuUDcWxBZF',
      accessKeySecret: 'Q8vYWEnbfoTgc2I2Ym2hqnbKhloQqw',
      endpoint: 'https://sts.aliyuncs.com',
      apiVersion: '2015-04-01',
    });

    this.clientParams = {
      RegionId: 'cn-chengdu',
      RoleArn: 'acs:ram::1993454754286660:role/ramossyiyun',
      RoleSessionName: 'yiyun-yijian',
    };
  }

  async getAliSecret() {
    try {
      const res = await this.client.request('AssumeRole', this.clientParams, {
        methods: 'POST',
      });
      return res;
    } catch (error) {
      return error;
    }
  }

  async getOSSFile(url) {
    const realUrl = decodeURIComponent(url);
    const req = http.get(realUrl, { responseType: 'arraybuffer' });
    const res = await req;
    if (!res || !res.data) return null;
    return {
      data: Buffer.from(res.data, 'binary').toString('base64'),
    };
  }
}
