import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Query } from '@nestjs/common';
import { CommonService } from './common.service';
import { OSSFileQueryDto } from './dto/oss.dto';

@ApiTags('Common')
@Controller('common')
export class CommonController {
  constructor(private readonly commonService: CommonService) {}

  @Get('alists')
  async getAliSecret() {
    const res = await this.commonService.getAliSecret();
    return res;
  }

  @Get('oss')
  async getOSSFile(@Query() query: OSSFileQueryDto) {
    const res = await this.commonService.getOSSFile(query.url);
    return res;
  }
}
