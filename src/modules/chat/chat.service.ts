import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Chat, ChatDocument } from './schema/chat.schema';
import { Model } from 'mongoose';
import {
  CreateChatDto,
  QueryChatRecordsDto,
  QueryChatRecordsResDto,
} from './dto/chat.dto';
import { OnlineUser, OnlineUserDocument } from './schema/onlineUser.schema';
import { UserService } from '../user/user.service';

@Injectable()
export class ChatService {
  constructor(
    @InjectModel(Chat.name) private readonly chatModel: Model<ChatDocument>,
    @InjectModel(OnlineUser.name)
    private readonly onlineUserModel: Model<OnlineUserDocument>,

    private readonly userService: UserService,
  ) {}

  async getOnlineUserIds() {
    const users = await this.onlineUserModel.find().exec();
    return users.map((user) => user.user);
  }

  async getOnlineUsers() {
    const users = await this.onlineUserModel
      .find({}, null, { sort: { updatedAt: -1 } })
      .populate('user')
      .exec();
    return users;
  }

  async createOnlineUser(userId: string) {
    const res = await this.onlineUserModel.updateOne(
      { user: userId },
      {},
      {
        upsert: true,
        new: true,
      },
    );
    return res;
  }

  async removeOnlineUser(userId: string) {
    const res = await this.onlineUserModel.findOneAndDelete({ user: userId });
    return res;
  }

  async createChatRecord(dto: CreateChatDto) {
    const users = await this.getOnlineUserIds();
    const res = await this.chatModel.create({
      ...dto,
      persons: users,
      timestamp: Date.now(),
    });
    const fixedRes = await (await res.populate('sender')).populate('reference');
    if (fixedRes.reference) {
      (<any>fixedRes.reference).sender = await this.userService.queryUserById(
        (<any>fixedRes.reference).sender as string,
      );
    }
    return fixedRes;
  }

  async joinRoom(userId: string) {
    const res = await this.createChatRecord({
      sender: userId,
      content: '#join',
    });
    await this.createOnlineUser(userId);
    return res;
  }

  async leaveRoom(userId: string) {
    const res = await this.createChatRecord({
      sender: userId,
      content: '#leave',
    });
    await this.removeOnlineUser(userId);
    return res;
  }

  async queryRecords(dto: QueryChatRecordsDto) {
    const data = await this.chatModel
      .find(
        {
          timestamp: { $lt: dto.timestamp },
          $nor: [{ content: '#join' }, { content: '#leave' }],
        },
        null,
        { sort: { timestamp: -1 } },
      )
      .skip(dto.start)
      .limit(dto.size)
      .populate('sender')
      .populate('reference')
      .exec();
    for (const item of data) {
      if (item.reference) {
        (<any>item.reference).sender = await this.userService.queryUserById(
          (<any>item.reference).sender as string,
        );
      }
    }
    const length = data?.length || 0;
    const res: QueryChatRecordsResDto = {
      start: dto.start,
      end: dto.start + length,
      size: dto.size,
      timestamp: dto.timestamp,
      finished: length < dto.size,
      data: data || [],
    };
    return res;
  }
}
