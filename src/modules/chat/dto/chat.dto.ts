export class CreateChatDto {
  sender: string;
  content: string;
  reference?: string;
}

export class ChatDto {
  _id: string;
  sender: string;
  persons: string[];
  content: string;
  reference?: string;
  timestamp: number;
}

export class QueryChatRecordsDto {
  start: number;
  size: number;
  timestamp: number;
}

export class QueryChatRecordsResDto {
  start: number;
  size: number;
  end: number;
  finished?: boolean;
  timestamp: number;
  data: ChatDto[];
}
