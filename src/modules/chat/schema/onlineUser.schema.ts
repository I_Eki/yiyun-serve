import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaType } from 'mongoose';

export type OnlineUserDocument = OnlineUser & Document;

@Schema({
  timestamps: true,
})
export class OnlineUser extends Document {
  @Prop({
    required: true,
    ref: 'User',
    type: SchemaType.Types.ObjectId,
    index: true,
    unique: true,
  })
  user: string;
}

export const OnlineUserSchema = SchemaFactory.createForClass(OnlineUser);
