import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaType } from 'mongoose';

export type ChatDocument = Chat & Document;

@Schema()
export class Chat extends Document {
  @Prop({
    required: true,
    ref: 'User',
    type: SchemaType.Types.ObjectId,
    index: true,
  })
  sender: string;

  @Prop({
    type: SchemaType.Types.Array,
    index: true,
    default: [],
  })
  persons: string[];

  @Prop({
    required: true,
    type: SchemaType.Types.String,
    index: true,
  })
  content: string;

  @Prop({
    type: SchemaType.Types.ObjectId,
    index: true,
    ref: 'Chat',
  })
  reference?: string;

  @Prop({
    required: true,
    type: SchemaType.Types.Number,
    index: true,
  })
  timestamp: number;
}

export const ChatSchema = SchemaFactory.createForClass(Chat);
