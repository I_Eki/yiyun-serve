import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Query } from '@nestjs/common';
import { ChatService } from './chat.service';
import { QueryChatRecordsDto } from './dto/chat.dto';

@ApiTags('Chat')
@Controller('chat')
export class ChatController {
  constructor(private readonly chatService: ChatService) {}

  @Get('/records')
  async queryRecords(@Query() query) {
    const res = await this.chatService.queryRecords({
      start: parseInt(query.start),
      size: parseInt(query.size),
      timestamp: parseInt(query.timestamp),
    } as QueryChatRecordsDto);
    return res;
  }

  @Get('/users')
  async getOnlineUsers() {
    const res = await this.chatService.getOnlineUsers();
    return res;
  }
}
