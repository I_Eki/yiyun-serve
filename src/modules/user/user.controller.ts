import { Body, Controller, Post, Put } from '@nestjs/common';
import { CreateUserDto } from './dto/user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('/')
  async create(@Body() body: CreateUserDto) {
    const res = await this.userService.createUser(body);
    return res;
  }

  @Put('/active')
  async active(@Body() body: IDDto) {
    const res = await this.userService.activeUser(body._id);
    return res;
  }
}
