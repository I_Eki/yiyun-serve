import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaType } from 'mongoose';

export enum UserStatus {
  CREATED,
  ACTIVED,
  DISABLED,
  DELETED,
}

export type UserDocument = User & Document;

@Schema()
export class User extends Document {
  @Prop({
    required: true,
    type: SchemaType.Types.String,
    index: true,
  })
  name: string;

  @Prop({
    required: true,
    type: SchemaType.Types.Number,
    index: true,
  })
  status: UserStatus;
}

export const UserSchema = SchemaFactory.createForClass(User);
