import { Injectable } from '@nestjs/common';
import { User, UserDocument, UserStatus } from './schema/user.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
  ) {}

  async queryUserById(_id: string) {
    const res = await this.userModel.findById(_id);
    return res;
  }

  async createUser(dto: CreateUserDto) {
    const res = await this.userModel.create({
      name: dto.name,
      status: dto.status || UserStatus.CREATED,
    });
    return res;
  }

  async activeUser(_id: string) {
    const res = await this.userModel
      .updateOne({ _id }, { status: UserStatus.ACTIVED })
      .exec();
    return res;
  }
}
