import { UserStatus } from '../schema/user.schema';

export class CreateUserDto {
  name: string;
  status?: UserStatus;
}

export class UserDto {
  _id: string;
  name: string;
  status: UserStatus;
}
