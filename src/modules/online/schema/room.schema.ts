import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaType } from 'mongoose';
import { PersonDto, Piece, RoomState } from '../dto/room.dto';

export type RoomDocument = Room & Document;

@Schema()
export class Room extends Document {
  @Prop({
    required: true,
    type: SchemaType.Types.String,
  })
  id: string;

  @Prop({
    required: true,
    type: SchemaType.Types.Number,
    default: RoomState.DEFAULT,
  })
  state: RoomState;

  @Prop({
    type: SchemaType.Types.Boolean,
    default: true,
  })
  isNextBlack: boolean;

  @Prop({
    type: SchemaType.Types.Array,
    default: [],
  })
  manual: Piece[];

  @Prop({
    type: SchemaType.Types.Array,
    default: [],
  })
  persons: PersonDto[];
}

export const RoomSchema = SchemaFactory.createForClass(Room);
