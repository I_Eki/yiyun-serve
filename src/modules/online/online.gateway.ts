import { ChatService } from './../chat/chat.service';
import { ChatDto, ChatType } from './dto/chat.dto';
import { JoinDto, LuoziDto, ReqDto } from './dto/room.dto';
import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { OnlineService } from './online.service';
import { CreateChatDto } from '../chat/dto/chat.dto';

const chatRoom = 'chat';

@WebSocketGateway(3540, {
  path: '/socket.io',
  transports: ['polling', 'websocket'],
})
export class OnlineGateway {
  @WebSocketServer() private ws: Server;
  constructor(
    private readonly onlineService: OnlineService,
    private readonly chatService: ChatService,
  ) {}

  @SubscribeMessage('join')
  async hanldeJoin(client: Socket, payload: JoinDto) {
    const res = await this.onlineService.joinRoom(payload);
    client.join(res.roomID);
    this.ws.to(res.roomID).emit('chat', {
      type: ChatType.NOTICE,
      text: `玩家<span class="name">${payload.name}</span>进入房间`,
    });
    return {
      event: 'joined',
      data: {
        code: 0,
        data: res,
      },
    };
  }

  @SubscribeMessage('prepare')
  async hanldePrepare(client: Socket, payload: string) {
    const { state, roomID, userName } = await this.onlineService.prepare(
      payload,
    );
    const adapter = this.ws.to(roomID);
    if (state === 0) {
      return {
        event: 'prepared',
        data: {
          code: -1,
          data: '尚未加入房间',
        },
      };
    } else if (state === 1) {
      adapter.emit('chat', {
        name: '',
        text: `玩家<span class="name">${userName}</span>已准备`,
        type: ChatType.NOTICE,
      });
      return {
        event: 'prepared',
        data: {
          code: 1,
          data: '',
        },
      };
    } else if (state === 2) {
      adapter.emit('chat', {
        name: '',
        text: `玩家<span class="name">${userName}</span>已准备`,
        type: ChatType.NOTICE,
      });
      setTimeout(() => {
        adapter.emit('start');
        adapter.emit('chat', {
          name: '',
          type: ChatType.NOTICE,
          text: '游戏开始!',
        });
        this.onlineService.startGameIn(roomID);
      }, 0);
      return {
        event: 'prepared',
        data: {
          code: 0,
          data: '',
        },
      };
    }
  }

  @SubscribeMessage('luozi')
  async hanldeLuozi(client: Socket, paylaod: LuoziDto) {
    const { roomID, piece } = paylaod;
    const isNextBlack = await this.onlineService.luozi(roomID, piece);
    const hasWinner = await this.onlineService.checkWinner(roomID);
    const adapter = this.ws.to(roomID);
    adapter.emit('luozi', {
      isNextBlack,
      piece,
    });

    if (hasWinner !== false) {
      adapter.emit('chat', {
        type: ChatType.NOTICE,
        text: `本局游戏结束，${isNextBlack ? '白' : '黑'}方胜利`,
      });
      adapter.emit('finish', {
        hasWinner,
        isNextBlack,
      });
    }
  }

  @SubscribeMessage('exited')
  async handleExited(client: Socket, payload: JoinDto) {
    const roomID = await this.onlineService.exitRoom(payload);
    client.leave(roomID);
    this.ws.to(roomID).emit('chat', {
      type: ChatType.NOTICE,
      text: `玩家<span class="name">${payload.name}</span>离开房间`,
    });
    return {
      event: 'exited',
      data: {
        code: roomID ? 0 : -1,
        data: roomID ? 'ok' : '退出失败',
      },
    };
  }

  @SubscribeMessage('pause')
  handleReqPause(client: Socket, payload: ReqDto) {
    const adapter = this.ws.to(payload.roomID);
    adapter.emit('req-pause', {
      id: payload.id,
      name: payload.name,
    });
    adapter.emit('chat', {
      type: ChatType.NOTICE,
      text: `玩家<span class="name">${payload.name}</span>请求暂停`,
    });
  }

  @SubscribeMessage('abort-pause')
  handleAbortPause(client: Socket, payload: ReqDto) {
    const adapter = this.ws.to(payload.roomID);
    adapter.emit('abort-pause', {
      id: payload.id,
      name: payload.name,
    });
    adapter.emit('chat', {
      type: ChatType.NOTICE,
      text: `玩家<span class="name">${payload.name}</span>不同意暂停`,
    });
  }

  @SubscribeMessage('agree-pause')
  handleAgreePause(client: Socket, payload: ReqDto) {
    this.onlineService.pause(payload.roomID);
    const adapter = this.ws.to(payload.roomID);
    adapter.emit('paused', {
      id: payload.id,
      name: payload.name,
    });
    adapter.emit('chat', {
      type: ChatType.NOTICE,
      text: '游戏暂停',
    });
  }

  @SubscribeMessage('continue')
  handleReqContinue(client: Socket, payload: ReqDto) {
    const adapter = this.ws.to(payload.roomID);
    adapter.emit('req-continue', {
      id: payload.id,
      name: payload.name,
    });
    adapter.emit('chat', {
      type: ChatType.NOTICE,
      text: `玩家<span class="name">${payload.name}</span>请求继续`,
    });
  }

  @SubscribeMessage('abort-continue')
  handleAbortContinue(client: Socket, payload: ReqDto) {
    const adapter = this.ws.to(payload.roomID);
    adapter.emit('abort-continue', {
      id: payload.id,
      name: payload.name,
    });
    adapter.emit('chat', {
      type: ChatType.NOTICE,
      text: `玩家<span class="name">${payload.name}</span>不同意继续`,
    });
  }

  @SubscribeMessage('agree-continue')
  handleAgreeContinue(client: Socket, payload: ReqDto) {
    this.onlineService.continue(payload.roomID);
    const adapter = this.ws.to(payload.roomID);
    adapter.emit('continue', {
      id: payload.id,
      name: payload.name,
    });
    adapter.emit('chat', {
      type: ChatType.NOTICE,
      text: '游戏继续',
    });
  }

  @SubscribeMessage('play-again')
  handleReqPlayAgain(client: Socket, payload: ReqDto) {
    const adapter = this.ws.to(payload.roomID);
    adapter.emit('req-play-again', {
      id: payload.id,
      name: payload.name,
    });
    adapter.emit('chat', {
      type: ChatType.NOTICE,
      text: `玩家<span class="name">${payload.name}</span>请求重来`,
    });
  }

  @SubscribeMessage('abort-play-again')
  handleAbortPlayAgain(client: Socket, payload: ReqDto) {
    const adapter = this.ws.to(payload.roomID);
    adapter.emit('abort-play-again', {
      id: payload.id,
      name: payload.name,
    });
    adapter.emit('chat', {
      type: ChatType.NOTICE,
      text: `玩家<span class="name">${payload.name}</span>不同意重来`,
    });
  }

  @SubscribeMessage('agree-play-again')
  handleAgreePlayAgain(client: Socket, payload: ReqDto) {
    this.onlineService.playAgain(payload.roomID);
    const adapter = this.ws.to(payload.roomID);
    adapter.emit('play-again', {
      id: payload.id,
      name: payload.name,
    });
    adapter.emit('chat', {
      type: ChatType.NOTICE,
      text: '新一局游戏已开始',
    });
  }

  @SubscribeMessage('chat')
  handleChat(client: Socket, payload: ChatDto) {
    this.ws.to(payload.roomID).emit('chat', {
      name: payload.name || '佚名',
      text: payload.text,
      type: payload.type ?? ChatType.MESSAGE,
    });
  }

  @SubscribeMessage('joinChat')
  async handleJoinChat(client: Socket, payload: any) {
    const res = await this.chatService.joinRoom(payload.userId);
    client.join(chatRoom);
    this.ws.to(chatRoom).emit('joinedChat', res);
  }

  @SubscribeMessage('leaveChat')
  async handleLeaveChat(client: Socket, payload: any) {
    const res = await this.chatService.leaveRoom(payload.userId);
    client.leave(chatRoom);
    this.ws.to(chatRoom).emit('leftChat', res);
  }

  @SubscribeMessage('talk')
  async handleTalk(client: Socket, payload: CreateChatDto) {
    const res = await this.chatService.createChatRecord(payload);
    this.ws.to(chatRoom).emit('talk', res);
  }
}
