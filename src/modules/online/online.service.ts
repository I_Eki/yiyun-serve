import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { getUUID } from 'src/utils/common';
import {
  RoomDto,
  JoinDto,
  JoinBackDto,
  RoomState,
  Piece,
} from './dto/room.dto';
import { Room, RoomDocument } from './schema/room.schema';

@Injectable()
export class OnlineService {
  // 方向基向量列表
  private static dirVectors = [
    [
      [-1, 0],
      [1, 0],
    ],
    [
      [0, -1],
      [0, 1],
    ],
    [
      [-1, -1],
      [1, 1],
    ],
    [
      [-1, 1],
      [1, -1],
    ],
  ];

  constructor(@InjectModel(Room.name) private roomModel: Model<RoomDocument>) {}

  async getRooms(): Promise<RoomDto[]> {
    const rooms = await this.roomModel.find({}).exec();
    return rooms as RoomDto[];
  }

  async findRoomByPersonID(id: string) {
    const rooms = await this.getRooms();
    return rooms.find((room) =>
      room.persons.some((person) => person.id === id),
    );
  }

  async createRoom(id = ''): Promise<RoomDto> {
    const persons = [];
    if (id) persons.push(id);

    const room = {
      id: getUUID(),
      persons,
      state: RoomState.DEFAULT,
    };

    const roomData = await this.roomModel.create(room);
    return roomData;
  }

  async joinRoom({ id, name, roomID = '' }: JoinDto): Promise<JoinBackDto> {
    if (!roomID) {
      const room = await this.createRoom(roomID);
      return {
        code: 0,
        roomID: room.id,
        isMeBlack: true,
      };
    }
    const rooms = await this.getRooms();
    const room = rooms.find((item) => item.id === roomID);

    // 房间不存在
    if (!room) {
      const roomData = await this.roomModel.create({
        id: roomID,
        state: RoomState.DEFAULT,
        persons: [
          {
            id,
            name,
            isBlack: true,
            prepared: false,
          },
        ],
      });
      return {
        code: 0,
        roomID: roomData.id,
        isMeBlack: true,
      };
    }

    // 房间已存在
    const person = room.persons.find((item) => item.id === id);
    if (person) {
      if (room.state !== RoomState.DEFAULT) {
        return {
          code: 0,
          roomID: room.id,
          isMeBlack: person.isBlack,
          person,
          room,
        };
      } else {
        return {
          code: 0,
          roomID: room.id,
          isMeBlack: person.isBlack,
        };
      }
    } else if (room.persons.length >= 2) {
      return {
        code: -2,
        message: '此房间已满',
        isMeBlack: false,
      };
    } else {
      const isMeBlack = room.persons.length < 1 || !room.persons[0].isBlack;
      room.persons.push({
        id,
        name,
        isBlack: isMeBlack,
        prepared: false,
      });
      await this.roomModel.updateOne({ _id: room._id }, room);
      return {
        code: 0,
        roomID: room.id,
        isMeBlack,
      };
    }
  }

  async startGameIn(id: string) {
    const rooms = await this.getRooms();
    const room = rooms.find((room) => room.id === id);
    room.state = RoomState.PLAYING;
    room.isNextBlack = true;
    room.manual = [];
    await this.roomModel.updateOne({ _id: room._id }, room).exec();
  }

  async exitRoom({ id, roomID = '' }: JoinDto) {
    const rooms = await this.getRooms();
    const room = rooms.find((item) => item.id === roomID);
    if (!room) return '';

    const personIndex = room.persons.findIndex((item) => item.id === id);
    if (personIndex > -1) {
      if (room.persons.length < 2) {
        await this.roomModel.deleteOne({ _id: room._id }).exec();
      } else {
        room.persons.splice(personIndex, 1);
        await this.roomModel
          .updateOne({ _id: room._id }, { persons: room.persons })
          .exec();
      }
    }

    return room.id;
  }

  async prepare(id: string) {
    let state = 0;
    let roomID = '';
    let userName = '';
    const rooms = await this.getRooms();
    for (const room of rooms) {
      const isSucceed = room.persons.some((person) => {
        if (person.id !== id) return;
        person.prepared = true;
        state = 1;
        roomID = room.id;
        userName = person.name;
        return true;
      });

      if (isSucceed) {
        if (
          room.persons.length >= 2 &&
          room.persons.every((person) => person.prepared)
        ) {
          state = 2;
        }

        await this.roomModel.updateOne({ _id: room._id }, room).exec();
      }
    }

    return {
      state,
      roomID,
      userName,
    };
  }

  async luozi(roomID: string, piece: Piece) {
    const rooms = await this.getRooms();
    const room = rooms.find((item) => item.id === roomID);
    room.manual.push(piece);
    room.isNextBlack = !room.isNextBlack;
    await this.roomModel.updateOne({ _id: room._id }, room);
    return room.isNextBlack;
  }

  async pause(roomID: string) {
    const rooms = await this.getRooms();
    const room = rooms.find((item) => item.id === roomID);
    room.state = RoomState.PAUSING;
    await this.roomModel.updateOne({ _id: room._id }, room);
    return room;
  }

  async continue(roomID: string) {
    const rooms = await this.getRooms();
    const room = rooms.find((item) => item.id === roomID);
    room.state = RoomState.PLAYING;
    await this.roomModel.updateOne({ _id: room._id }, room);
    return room;
  }

  async checkWinner(roomID: string) {
    const rooms = await this.getRooms();
    const room = rooms.find((item) => item.id === roomID);
    if (room.manual.length >= 19 ** 2) {
      room.state = RoomState.FINISHED;
      await this.roomModel.updateOne({ _id: room._id }, room);
      return null;
    }
    let count = 1;
    const lastStep = room.manual[room.manual.length - 1];
    const { x, y, val } = lastStep;
    const hasWinner = OnlineService.dirVectors.some((vectorPair) => {
      count = 1;
      vectorPair.forEach(([vx, vy]) => {
        for (let i = 1; i < 5; i++) {
          const tx = x + vx * i;
          const ty = y + vy * i;
          if (
            !room.manual.some((i) => i.x === tx && i.y === ty && i.val === val)
          )
            break;
          count++;
        }
      });
      return count >= 5;
    });

    if (hasWinner) {
      room.state = RoomState.FINISHED;
      await this.roomModel.updateOne({ _id: room._id }, room);
    }

    return hasWinner;
  }

  async playAgain(roomID: string) {
    const rooms = await this.getRooms();
    const room = rooms.find((item) => item.id === roomID);
    room.state = RoomState.PLAYING;
    room.isNextBlack = true;
    room.manual = [];
    await this.roomModel.updateOne({ _id: room._id }, room);
    return room;
  }
}
