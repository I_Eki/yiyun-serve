export enum ChatType {
  MESSAGE,
  NOTICE,
}

export class ChatDto {
  id: string;
  name: string;
  roomID: string;
  text: string;
  type?: ChatType;
}
