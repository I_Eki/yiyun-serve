export enum RoomState {
  DEFAULT,
  PLAYING,
  PAUSING,
  FINISHED,
}

export class Piece {
  x: number;
  y: number;
  val: boolean;
}

export class PersonDto {
  id: string;
  name: string;
  isBlack: boolean;
  prepared: boolean;
}

export class RoomDto {
  _id: string;
  id: string;
  state: RoomState;
  isNextBlack?: boolean;
  manual?: Piece[];
  persons: PersonDto[];
}

export class JoinDto {
  id: string;
  name: string;
  roomID?: string;
}

export class JoinBackDto {
  code: 0 | -1 | -2;
  roomID?: string;
  isMeBlack: boolean;
  message?: string;
  person?: PersonDto;
  room?: RoomDto;
}

export class LuoziDto {
  roomID: string;
  piece: Piece;
}

/** 请求暂停/继续的数据结构 */
export class ReqDto {
  id: string;
  name: string;
  roomID: string;
}
