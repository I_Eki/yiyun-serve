import { Module } from '@nestjs/common';
import { OnlineService } from './online.service';
import { OnlineGateway } from './online.gateway';
import { MongooseModule } from '@nestjs/mongoose';
import { Room, RoomSchema } from './schema/room.schema';
import { ChatModule } from './../chat/chat.module';

@Module({
  imports: [
    ChatModule,
    MongooseModule.forFeature([
      {
        name: Room.name,
        schema: RoomSchema,
      },
    ]),
  ],
  providers: [OnlineService, OnlineGateway],
  controllers: [],
})
export class OnlineModule {}
