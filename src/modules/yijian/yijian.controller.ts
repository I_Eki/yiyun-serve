import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { YijianService } from './yijian.service';

@ApiTags('Yijian')
@Controller('yijian')
export class YijianController {
  constructor(private readonly yjService: YijianService) {}

  @Post()
  transmit(@Body() body) {
    this.yjService.transmit(body);
    return 'ok';
  }
}
