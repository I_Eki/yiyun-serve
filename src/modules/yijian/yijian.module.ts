import { Module } from '@nestjs/common';
import { YijianController } from './yijian.controller';
import { YijianService } from './yijian.service';

@Module({
  providers: [YijianService],
  controllers: [YijianController],
})
export class YiJianModule {}
