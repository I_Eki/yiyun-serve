import { Injectable } from '@nestjs/common';
import * as Pusher from 'pusher';

@Injectable()
export class YijianService {
  private readonly pusher: Pusher;

  constructor() {
    this.pusher = new Pusher({
      appId: '1332780',
      key: '2a08da470d790d35b529',
      secret: '57505ec5693ec0ad309f',
      cluster: 'ap3',
    });
  }

  transmit(data) {
    this.pusher.trigger('yiyun-serve', 'transmit', data);
  }
}
