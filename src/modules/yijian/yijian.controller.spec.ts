import { Test, TestingModule } from '@nestjs/testing';
import { YijianController } from './yijian.controller';

describe('YijianController', () => {
  let controller: YijianController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [YijianController],
    }).compile();

    controller = module.get<YijianController>(YijianController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
