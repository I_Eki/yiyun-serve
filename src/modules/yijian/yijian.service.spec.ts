import { Test, TestingModule } from '@nestjs/testing';
import { YijianService } from './yijian.service';

describe('YijianService', () => {
  let service: YijianService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [YijianService],
    }).compile();

    service = module.get<YijianService>(YijianService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
