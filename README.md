<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

<p>本项目使用 <a href="https://circleci.com/gh/nestjs/nest" target="_blank">Nest</a> 创建</p>

## 描述

[亦云阁](https://yiyun.eki.space/) 是个人建站项目，开放源码仅供学习参考使用.

## 安装

```bash
$ yarn install
```

## 环境搭建

+ Mongo数据库账户密码设置

1. 请先保证 `Mongo` 服务正常运行；
2. 请下载并安装[MongoShell](https://www.mongodb.com/try/download/shell)，如果已安装，跳过此步骤。
3. 打开命令提示符或 `PowerShell` 窗口，依次运行以下命令：

```bash
$ mongosh
$ use yiyun-games
$ db.createUser({user:"yiyun",pwd:passwordPrompt(),roles:[{role:"dbOwner",db:"yiyun-games"}]})
```

执行 `db.createUser` 命令后，窗口会等待输入密码，此时手动输入 `Eki0)3#5%4$` 并回车，如果看到 `{ ok: 1 }` ，则说明用户密码创建成功。

## 运行

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## 测试

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## 支持

## 联系我

- Author - [Eki](https://yiyun.eki.space/)
- Email - ichinoseeki@outlook.com

## License

[MIT licensed](LICENSE).
